import unittest

import HtmlTestRunner
import testscenario.news_main as news
import testscenario.login as login

""" 테스트 로더로 테스트 케이스 생성 """
scenario_1 = unittest.TestLoader().loadTestsFromTestCase(login.LoginTest)
scenario_2 = unittest.TestLoader().loadTestsFromTestCase(news.NewsTest)

""" 테스트 스위트 구성 """
testsuite = unittest.TestSuite([scenario_1, scenario_2])

""" 테스트 러너로 테스트 실행 """
HtmlTestRunner.HTMLTestRunner(
                              output="BaseAppium/reports",      # 보고서 생성할 폴더 이름
                              report_name="Report",     # HTML 파일 이름
                              report_title="Test Results",   # HTML 파일 타이틀
                              combine_reports=True  # True : 테스트 케이스가 여러개 일 때 보고서 하나로 합치기
                              ).run(testsuite)
