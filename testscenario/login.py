from utils.appiumdriver import Driver
from page.morepage import MorePage
from page.newsmain import NewsMain
import time


class LoginTest(Driver):
    def testcase_1(self):
        # main 화면 객체 생성
        newsmain = NewsMain(self.driver)
        # more 화면 객체 생성
        morepage = MorePage(self.driver)

        newsmain.click_more_btn()       # 더보기 화면 진입

        morepage.click_sign_btn()

        morepage.input_id_btn()

        morepage.input_pwd_btn()

        # morepage.click_login_btn()
        """ login 관련 계정 및 화면 별 진입 검증을 위한 assert 구문 추가 검토 """

        morepage.click_cancel_btn()

        morepage.click_news_btn()
        time.sleep(2)

        newsmain.click_more_btn()

        morepage.click_bookmark_btn()
        time.sleep(2)

        newsmain.click_more_btn()

        morepage.click_comments_btn()
        time.sleep(2)

        newsmain.click_more_btn()

        morepage.click_setting_btn()
