from utils.appiumdriver import Driver
from page.newsmain import NewsMain
import time


class NewsTest(Driver):
    def testcase_1(self):
        # 메인 화면 객체 생성
        newsmain = NewsMain(self.driver)

        # Top Stories 기사 확인
        newsmain.click_article_btn()    # 첫번째 기사 확인
        newsmain.click_close_btn()  # 뒤로 이동
        newsmain.click_share_btn()  # 공유 버튼 동작 확인
        newsmain.click_copy_btn()   # 공유 버튼 > 복사
        newsmain.click_bookmark_btn()   # 첫번째 기사 즐겨찾기 추가
        # newsmain.click_comments_btn()   # 기사의 댓글 확인 *** 기사 댓글 진입 시 (가상환경 사양 문제로 추측) 무한 로딩되어 주석 처리
        # newsmain.click_more_btn()   # 뒤로가기
        newsmain.click_bookmark_btn()   # 첫번째 기사 즐겨찾기 해제
        # newsmain.click_external_btn()  # webview 진입 뒤로가기 불가로 테스트 주석처리

        # Newest 기사 확인
        newsmain.click_newest_btn() # newest 탭으로 이동
        time.sleep(2)
        newsmain.click_article_btn()    # 첫번째 기사 확인
        newsmain.click_close_btn()  # 뒤로 이동
        newsmain.click_share_btn()  # 공유 버튼 동작 확인
        newsmain.click_copy_btn()   # 공유 버튼 > 복사
        newsmain.click_bookmark_btn()   # 첫번째 기사 즐겨찾기 추가
        newsmain.click_bookmark_btn()  # 첫번째 기사 즐겨찾기 해제

        # Best 기사 확인
        newsmain.click_best_btn()
        time.sleep(2)
        newsmain.click_article_btn()    # 기사 확인
        newsmain.click_close_btn()  # 뒤로 이동
        newsmain.click_share_btn()  # 공유 버튼 동작 확인
        newsmain.click_copy_btn()   # 공유 버튼 > 복사
        newsmain.click_bookmark_btn()   # 첫번째 기사 즐겨찾기 추가
        newsmain.click_bookmark_btn()  # 첫번째 기사 즐겨찾기 해제

        # Show HN 기사 확인
        newsmain.click_show_btn()
        time.sleep(2)
        newsmain.click_article_btn()    # 첫번째 기사 확인
        newsmain.click_close_btn()  # 뒤로 이동
        newsmain.click_share_btn()  # 공유 버튼 동작 확인
        newsmain.click_copy_btn()   # 공유 버튼 > 복사
        newsmain.click_bookmark_btn()   # 첫번째 기사 즐겨찾기 추가
        newsmain.click_bookmark_btn()  # 첫번째 기사 즐겨찾기 해제

        # ASK HN 기사확인
        newsmain.click_ask_btn()
        time.sleep(2)
        newsmain.click_article_btn()    # 첫번째 기사 확인 *** Loading 관련 Fail 발생으로 확인 필요
        newsmain.click_close_btn()  # 뒤로 이동
        newsmain.click_share_btn()  # 공유 버튼 동작 확인
        newsmain.click_copy_btn()   # 공유 버튼 > 복사
        newsmain.click_bookmark_btn()   # 첫번째 기사 즐겨찾기 추가
        newsmain.click_bookmark_btn()  # 첫번째 기사 즐겨찾기 해제

        # JOBS 기사 확인
        newsmain.click_jobs_btn()
        time.sleep(2)
        newsmain.click_article_btn()    # 첫번째 기사 확인
        newsmain.click_close_btn()  # 뒤로 이동
        newsmain.click_share_btn()  # 공유 버튼 동작 확인
        newsmain.click_copy_btn()   # 공유 버튼 > 복사
        newsmain.click_bookmark_btn()   # 첫번째 기사 즐겨찾기 추가
        newsmain.click_bookmark_btn()  # 첫번째 기사 즐겨찾기 해제
