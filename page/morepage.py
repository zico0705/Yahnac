from selenium.webdriver.common.by import By
# 더보기 화면의 버튼 구현


class BasePage(object):
    """각 페이지 오브젝트 초기화시켜주는 BasePage 클래스 구현"""
    def __init__(self, driver):
        self.driver = driver


class MorePage(BasePage):
    """More Page 오브젝트 생성"""
    btn_news = (By.XPATH, "//*[text()='News']")

    # 더보기 화면에의 버튼 메서드 구현

    def click_back_btn(self):   # 뒤로가기 버튼
        self.driver.find_element_by_xpath('//android.widget.ImageButton[@content-desc="Navigate up"]').click()

    def click_sign_btn(self):   # 로그인 화면 진입 버튼
        self.driver.find_element_by_id("com.malmstein.yahnac:id/view_drawer_header_login").click()

    def input_id_btn(self):     # ID 입력
        self.driver.find_element_by_id("com.malmstein.yahnac:id/login_username").send_keys("Name")

    def input_pwd_btn(self):    # PW 입력
        self.driver.find_element_by_id("com.malmstein.yahnac:id/login_password").send_keys("Password")

    def click_cancel_btn(self):     # 취소 버튼
        self.driver.find_element_by_id("com.malmstein.yahnac:id/login_cancel").click()

    def click_login_btn(self):      # 로그인 버튼
        self.driver.find_element_by_id("com.malmstein.yahnac:id/login_login").click()

    def click_news_btn(self):       # 더보기 > news 탭
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.support.v7.widget.LinearLayoutCompat[1]/android.widget.CheckedTextView").click()

    def click_bookmark_btn(self):       # 더보기 > 즐겨찾기 탭
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.support.v7.widget.LinearLayoutCompat[2]/android.widget.CheckedTextView").click()

    def click_comments_btn(self):       # 더보기 > comments 탭
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.support.v7.widget.LinearLayoutCompat[3]/android.widget.CheckedTextView").click()
    """ element 확인이 어려운 상태 """

    def click_setting_btn(self):        # 더보기 > setting 탭
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.widget.FrameLayout/android.support.v7.widget.RecyclerView/android.support.v7.widget.LinearLayoutCompat[4]/android.widget.CheckedTextView").click()
