
# Main 화면의 버튼 구현

class BasePage(object):
    """각 페이지 오브젝트 초기화시켜주는 BasePage 클래스 구현"""
    def __init__(self, driver):
        self.driver = driver


class NewsMain(BasePage):
    """Main Page 오브젝트 생성"""

    # Main 화면에의 버튼 클릭 메서드 구현
    def click_more_btn(self):   # 더보기 버튼
        self.driver.find_element_by_xpath('//android.widget.ImageButton[@content-desc="Navigate up"]').click()

    def click_top_btn(self):    # TOP STORIES 항목
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.support.v7.app.ActionBar.Tab[1]").click()

    def click_newest_btn(self):     # NEWEST 항목
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.support.v7.app.ActionBar.Tab[2]").click()

    def click_best_btn(self):   # BEST 항목
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.support.v7.app.ActionBar.Tab[3]").click()

    def click_show_btn(self):   # SHOW HN 항목
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.support.v7.app.ActionBar.Tab[4]").click()

    def click_ask_btn(self):    # ASK HN 항목
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.support.v7.app.ActionBar.Tab[4]").click()

    def click_jobs_btn(self):   # JOBS 항목
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.widget.LinearLayout[1]/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.support.v7.app.ActionBar.Tab[5]").click()

    def click_article_btn(self):    # 기사 상세보기
        self.driver.find_element_by_id("com.malmstein.yahnac:id/article_title").click()

    def click_close_btn(self):  # 닫기 버튼
        self.driver.find_element_by_id("com.android.chrome:id/close_button").click()

    def click_bookmark_btn(self):   # 즐겨찾기 버튼
        self.driver.find_element_by_id("com.malmstein.yahnac:id/article_bookmark_action").click()

    def click_comments_btn(self):   # comments 버튼
        self.driver.find_element_by_id("com.malmstein.yahnac:id/article_comments_label").click()

    def click_external_btn(self):   # 기사 웹 브라우저로 열기
        self.driver.find_element_by_id("com.malmstein.yahnac:id/article_external_action").click()

    def click_share_btn(self):  # 공유하기 버튼
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.support.v4.view.ViewPager/android.widget.FrameLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[1]/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.ImageButton[1]").click()

    def click_copy_btn(self):   # 공유하기 > 복사 버튼
        self.driver.find_element_by_id("android:id/chooser_copy_button").click()

    def click_best_article_btn(self):   # best 기사 xpath 진입
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.support.v4.view.ViewPager/android.widget.FrameLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView").click()

    def click_show_article_btn(self):   # show HN 기사 xpath 진입
        self.driver.find_element_by_xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.support.v4.widget.DrawerLayout/android.view.ViewGroup/android.support.v4.view.ViewPager/android.widget.FrameLayout/android.view.ViewGroup/android.support.v7.widget.RecyclerView/android.widget.FrameLayout[2]/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView").click()